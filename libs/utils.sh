#!/usr/bin/env bash

print_usage() {
    cat <<- EOF

    usage: ${PROG_NAME} parameters options

    Generate a docker image with the configuration in a specific volume.
    Publish the resulting image to Docker Store

    MANDATORY PARAMETERS:
       -g --git-repository           url of the git repository holding the configuration
       -v --env-version              configuration version to retrieve
       -e --env-name                 name of the env-name to generate the configuration for

    OPTIONS:
       -h --help                     print this help
       -w --working-directory        specify the working directory inside the image (optional)
                                      (default value: /var/tmp)
       -f --config-file-name         name of the output file generated
                                      (default value: env.sh)
       -d --config-base-directory    folder where the generated file will be generated
                                      (default value: /config)
       -i --image-name               docker image name (pushed with a tag associated to the env and the version values)
                                      (default value: env-data:\$env--\$version)
       -p --publish-image            publish the image to the associated docker registry
       -n --no-cache                 bypass docker build cache
       -q --quiet                    Suppress output and print image ID on success

    Examples:
       ${PROG_NAME} -e dev-local -v v0.1.0 --git-repository https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git
EOF
}

print_state() {
    if [ "${QUIET}" == "no" ] ; then
        cat <<- EOF

    ##################################################################
    ${PROG_NAME}: $1
    $2 $3
    ##################################################################
EOF
    fi
}

cmd_building_build_docker_image() {
    local date
    date=$1
    local arguments=""
    if [ -n "${DOCKER_CACHE_OPTION}" ]; then
        arguments="${arguments} ${DOCKER_CACHE_OPTION}"
    fi
    arguments="${arguments} -t ${FULL_IMAGE_NAME}"
    arguments="${arguments} --build-arg WORKDIR=${WORK_DIR}"
    arguments="${arguments} --build-arg OUTDIR=${OUT_DIR}"
    arguments="${arguments} --build-arg REPOSITORY=${REPO}"
    arguments="${arguments} --build-arg VERSION=${ENV_VERSION}"
    arguments="${arguments} --build-arg ENVIRONMENT=${ENV_NAME}"
    arguments="${arguments} --build-arg CONFIG_FILE_NAME=${OUTPUT_FILE_CONFIG_NAME}"
    arguments="${arguments} --build-arg BUILD_DATE=${date}"
    arguments="${arguments} ."
    echo "docker build ${arguments}"
}

build_docker_image() {
    print_state "building data image..." "$FUNCNAME" "$@"

    local date
    date=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
    if [ "${QUIET}" == "no" ] ; then
        set -x
        eval "$(cmd_building_build_docker_image "${date}")"
        set +x
    else
        eval "$(cmd_building_build_docker_image "${date}") --quiet"
    fi
    print_state "Docker image:" "${FULL_IMAGE_NAME} created!" ""
}

cmd_building_publish_docker_image() {
    local image_name=$1
    echo "docker push ${image_name}"
}

publish_docker_image() {
    print_state "pushing data image" "$FUNCNAME" "$@"
    if [ "${PUSH_IMAGE}" == "yes" ]; then
        eval "$(cmd_building_publish_docker_image "${FULL_IMAGE_NAME}")" \
            || exit
        print_state "Docker ${FULL_IMAGE_NAME} image published!" "" ""
    else
        print_state "Skipping image push as requested" "(use '-p/--publish' to push to a registry)" ""
    fi
}

check_initialized() {
    print_state "test if $1 option is initialised" "$FUNCNAME" "$@"
    local name=$1
    local value=$2
    if ! [ -n "$value" ]; then
        echo -e "\n$name: $value is mandatory!"
        print_usage
        exit 1
    fi
}

args_manager() {
    local arg=
    for arg
    do
        local delim=""
        case "$arg" in
            #translate --gnu-long-options to -g (short options)
            --help)             print_usage && exit 0;;
            --git-repository)           args="${args}-g ";;
            --env-version)              args="${args}-v ";;
            --env-name)                 args="${args}-e ";;
            --working-directory)        args="${args}-w ";;
            --config-file-name)         args="${args}-f ";;
            --config-base-directory)    args="${args}-d ";;
            --image-name)               args="${args}-i ";;
            --publish-image)            args="${args}-p ";;
            --no-cache)                 args="${args}-n ";;
            --quiet)                    args="${args}-q ";;
            #pass through anything else
            *) [[ "${arg:0:1}" == "-" ]] || delim="\""
                args="${args}${delim}${arg}${delim} ";;
        esac
    done

    #Reset the positional parameters to the short options
    eval set -- "$args"

    while getopts ":hqpng:v:e:w:f:d:i:" OPTION
    do
         case $OPTION in
         h)
             print_usage
             exit 0
             ;;
         q)
             readonly QUIET=yes
             ;;
         p)
             readonly PUSH_IMAGE=yes
             ;;
         n)
             readonly DOCKER_CACHE_OPTION=--no-cache
             ;;
         g)
             readonly REPO=$OPTARG
             ;;
         v)
             readonly ENV_VERSION=$OPTARG
             ;;
         e)
             readonly ENV_NAME=$OPTARG
             ;;
         w)
             WORK_DIR=$OPTARG
             ;;
         f)
             OUTPUT_FILE_CONFIG_NAME=$OPTARG
             ;;
         d)
             OUT_DIR=$OPTARG
             ;;
         i)
             IMAGE_NAME=$OPTARG
             ;;
         :)
            echo -e "$OPTARG: missing mandatory value"
            print_usage
            exit 1
            ;;
         \?)
             if [ "$OPTARG" == "-" ]
             then
                echo -e "long option unknown"
             else
                echo -e "$OPTARG : option unknown"
             fi
             print_usage
             exit 1
             ;;
        esac
    done

    shift $((OPTIND-1))
    if [ $# -ne 0 ]; then
        echo -e "too many arguments: $*"
        print_usage
        exit 1
    fi

    check_initialized "${!REPO@}" "${REPO}"
    check_initialized "${!ENV_VERSION@}" "${ENV_VERSION}"
    check_initialized "${!ENV_NAME@}" "${ENV_NAME}"

    FULL_IMAGE_NAME=${IMAGE_NAME}:${ENV_NAME}--${ENV_VERSION}
}
