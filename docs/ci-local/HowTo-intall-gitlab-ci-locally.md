# How to install *Gitlab CI* locally
 
## Purpose

*GitLab CI* is the tool connected to GitLab to run Continuous Integration. 
To put CI into your project you must edit a file at the root of the project named: *.gitlab-ci.yml*.
Inside this file, you describe how to run jobs to test your application.
A very useful feature of *GitLab CI* is you can run your jobs inside **Docker image**.
Docker helps you to don't thing too much about the configuration of your environment.

By this way, you can run your test locally, in a **Docker image**, via *GitLab CI*.

The name of this soft is: *gitlab-ci-multi-runner*

## Via command line

This tutorial is a copy from [the official tutorial](https://docs.gitlab.com/runner/install/linux-repository.html).
In case of any question, please look at it. 

### Ubuntu
```bash
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-ci-multi-runner
```

### Centos
```bash
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.rpm.sh | sudo bash
sudo yum install gitlab-ci-multi-runner
```

### Fedora
There is a [available package list](https://packages.gitlab.com/runner/gitlab-ci-multi-runner) for each configuration.
```bash
curl -s https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.rpm.sh | sudo bash
sudo yum install gitlab-ci-multi-runner-[version].[architecture]
```

