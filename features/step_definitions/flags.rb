require 'securerandom'
require 'uri'
require 'net/http'
require 'json'
require 'openssl'

########################################################################################################################
# GIVEN
########################################################################################################################

Given(/^I initialized options$/) do
  @cmd = ''
end

Given(/^I added flag "([^"]*)"$/) do |flag|
  @cmd = @cmd + ' ' + flag
end

Given(/^I initialized mandatory options$/) do
  step 'I added flag "-g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"'
  step 'I added flag "-v v0.1.0"'
  step 'I added flag "-e dev-local"'
end

Given(/^I was logged$/) do
  raise('Invalid docker login') unless (`docker login -u $USERNAME -p $PASSWORD`.include? 'Login Succeeded')
end

Given(/^I generated image name$/) do
  @image_name = 'nodevops/' + SecureRandom.uuid
  @cmd = @cmd + ' -i' + @image_name
end

Given(/^I checked if the name is single$/) do
  raise('TODO') if (`docker search #{@image_name}`.include? @image_name)
end

Given(/^I have already loaded the docker cache$/) do
  step 'I initialized options'
  step 'I initialized mandatory options'
  step 'I launch generate-data-image.sh'
  step 'I should receive program exit with status "0"'
end

########################################################################################################################
# WHEN
########################################################################################################################

When(/^I launch generate-data-image\.sh$/) do
  @output = `./generate-data-image.sh #{@cmd}`
  @success = $?.success?
end

########################################################################################################################
# THEN
########################################################################################################################

Then(/^I should receive program exit with status "([^"]*)"$/) do |exit_status|
  raise('Invalid exit status') unless ((@success ? 0 : 1) == exit_status.to_i)
end

Then(/^I should receive only the sha of the new image$/) do
  raise('Invalid sha256 for quiet option') unless (@output =~ /^sha256:[0-9,a-z]{64}$/)
end

Then(/^I should receive the help message$/) do
  raise('Invalid output: no usage printed') unless (@output.include? 'usage')
end

Then(/^I should receive an error message "([^"]*)"$/) do |message|
  raise('Invalid output: no error printed') unless (@output.include? message)
end

Then(/^I should receive trace message$/) do
  raise('Invalid status: at least one script failed!') unless (@output.include? 'Data merged!') && (@output.include? 'Successfully built')
end

Then(/^I should see docker cache used$/) do
  raise('Invalid docker cache not used') unless (@output.include? '---> Using cache')
end

Then(/^I should not see usage of docker cache$/) do
  raise('Invalid docker cache used') if (@output.include? '---> Using cache')
end

Then(/^I should be able to pull this image$/) do
  raise('Invalid pull request for new image') unless (`docker pull #{@image_name}:dev-local--v0.1.0`.include? 'Pulling from')
end

Then(/^I have to delete it$/) do
  url = URI('https://hub.docker.com/v2/users/login/')

  http = Net::HTTP.new(url.host, url.port)
  http.use_ssl = true
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE

  request = Net::HTTP::Post.new(url)
  request['content-type'] = 'application/json'
  request.body = '{"username": "' + ENV['USERNAME'] + '","password": "' + ENV['PASSWORD'] + '"}'

  response = http.request(request)
  token = JSON.parse(response.read_body)['token']

  url = URI('https://hub.docker.com/v2/repositories/'+@image_name+'/')

  http = Net::HTTP.new(url.host, url.port)
  http.use_ssl = true
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE

  request = Net::HTTP::Delete.new(url)
  request['authorization'] = 'JWT '+ token

  response = http.request(request)
  raise('Invalid delete operation on Docker Hub for '+@image_name) unless (response.read_body == '')
end

Then(/^I must be logged out$/) do
  raise('Invalid docker logout') unless (`docker logout`.include? 'Removing login credentials for https://index.docker.io/v1/')
end
