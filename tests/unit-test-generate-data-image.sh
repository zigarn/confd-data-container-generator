#!/usr/bin/env bash

source ../libs/utils.sh

test_check_initialized() {
    VARIABLE=
    assertFalse "$FUNCNAME: Not initialized"     "check_initialized ${!VARIABLE@} ${VARIABLE}"
    VARIABLE="test"
    assertTrue "$FUNCNAME: Initialized"          "check_initialized ${!VARIABLE@} ${VARIABLE}"
}

test_args_manager() {
    # shellcheck disable=SC2034
    ENV_NAME=
    # shellcheck disable=SC2034
    ENV_VERSION=
    # shellcheck disable=SC2034
    REPO=
    #TODO: update when change one flag
    # Valid flags
    assertTrue "$FUNCNAME: Short flag: help"                    "args_manager -h"
    assertTrue "$FUNCNAME: Long flag: help"                     "args_manager --help"
    assertTrue "$FUNCNAME: example command short options"       "args_manager -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: example command long options"        "args_manager --env-name dev-local --env-version v0.1.0 --git-repository https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"

    assertTrue "$FUNCNAME: Short flag: working-directory"       "args_manager -w ./tmp                          -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: Long flag: working-directory"        "args_manager --working-directory ./tmp         -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: Short flag: config-file-name"        "args_manager -f env.sh                         -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: Long flag: config-file-name"         "args_manager --config-file-name env.sh         -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: Short flag: config-base-directory"   "args_manager -d ./config                       -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: Long flag: config-base-directory"    "args_manager --config-base-directory ./config  -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: Short flag: image-name"              "args_manager -i toto                           -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: Long flag: image-name"               "args_manager --image-name toto                 -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: Short flag: publish-image"           "args_manager -p                                -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: Long flag: publish-image"            "args_manager --publish-image                   -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: Short flag: no-cache"                "args_manager -n                                -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: Long flag: no-cache"                 "args_manager --no-cache                        -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: Short flag: quiet"                   "args_manager -q                                -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: Long flag: quiet"                    "args_manager --quiet                           -e dev-local -v v0.1.0 -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    # Invalid flags
    assertFalse "$FUNCNAME: Short flag: unknown"                "args_manager -k"
    assertFalse "$FUNCNAME: Long flag: unknown"                 "args_manager --unknown-flag"
}

test_cmd_building_build_docker_image() {
    local date
    date=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
    assertNotEquals "docker build"      "$(cmd_building_build_docker_image "${date}")"
    DOCKER_CACHE_OPTION=
    FULL_IMAGE_NAME=test-image
    WORK_DIR=/temp
    OUT_DIR=/data
    REPO=https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git
    ENV_VERSION=v0.1.0
    ENV_NAME=dev-local
    OUTPUT_FILE_CONFIG_NAME=env.sh
    assertEquals "docker build  -t ${FULL_IMAGE_NAME} --build-arg WORKDIR=${WORK_DIR} --build-arg OUTDIR=${OUT_DIR} --build-arg REPOSITORY=${REPO} --build-arg VERSION=${ENV_VERSION} --build-arg ENVIRONMENT=${ENV_NAME} --build-arg CONFIG_FILE_NAME=${OUTPUT_FILE_CONFIG_NAME} --build-arg BUILD_DATE=${date} ."                             "$(cmd_building_build_docker_image "${date}")"
    DOCKER_CACHE_OPTION=--no-cache
    assertEquals "docker build  ${DOCKER_CACHE_OPTION} -t ${FULL_IMAGE_NAME} --build-arg WORKDIR=${WORK_DIR} --build-arg OUTDIR=${OUT_DIR} --build-arg REPOSITORY=${REPO} --build-arg VERSION=${ENV_VERSION} --build-arg ENVIRONMENT=${ENV_NAME} --build-arg CONFIG_FILE_NAME=${OUTPUT_FILE_CONFIG_NAME} --build-arg BUILD_DATE=${date} ."       "$(cmd_building_build_docker_image "${date}")"
}

test_cmd_building_publish_docker_image(){
    assertEquals    "docker push image-test"    "$(cmd_building_publish_docker_image image-test)"
    assertNotEquals "docker push fail"          "$(cmd_building_publish_docker_image image-test)"
}

# load shunit2
. shunit2
