#!/bin/bash

readonly PROG_NAME=$(basename "$0")
readonly ARGS=( "$@" )

source libs/utils.sh

function main() {
    args_manager "${ARGS[@]}"

    print_state "runtime configuration"
    if [ "${QUIET}" == "no" ] ; then
        cat <<- EOF
    ENV_NAME=${ENV_NAME}
    ENV_VERSION=${ENV_VERSION}
    REPO=${REPO}
    WORK_DIR=${WORK_DIR}
    OUT_DIR=${OUT_DIR}
    OUTPUT_FILE_CONFIG_NAME=${OUTPUT_FILE_CONFIG_NAME}
    IMAGE_NAME=${IMAGE_NAME}
    FULL_IMAGE_NAME=${FULL_IMAGE_NAME}
    PUSH_IMAGE=${PUSH_IMAGE}
    DOCKER_CACHE_OPTION=${DOCKER_CACHE_OPTION}
    QUIET=${QUIET}
EOF
    fi

    build_docker_image "" \
        || exit

    publish_docker_image "" \
        || exit
}

###########################################################################################
# main
###########################################################################################

ENV_NAME=
ENV_VERSION=
REPO=

# default global variables
WORK_DIR=/var/tmp/
OUT_DIR=/config
OUTPUT_FILE_CONFIG_NAME=env.sh
IMAGE_NAME=env-data
FULL_IMAGE_NAME=
PUSH_IMAGE=no
DOCKER_CACHE_OPTION=
QUIET=no

main
